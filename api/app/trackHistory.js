const express = require('express');
const auth = require('../middleware/auth');
const TrackHistory = require('../models/TrackHistory');

const router = express.Router();

router.post('/', auth, async (req, res, next) => {
  try {

    if(!req.body.track) {
      return res.status(404).send('Track is required');
    }

    const dateTime = new Date().toISOString();

    const trackHistoryData = {
      user: req.user,
      track: req.body.track,
      datetime: dateTime
    };

    const trackHistory = new TrackHistory(trackHistoryData);

    await trackHistory.save();

    return res.send(trackHistory);

  } catch (e) {
    return next(e);
  }
});

module.exports = router;