import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ArtistsComponent} from "./pages/artists/artists.component";
import {AlbumsComponent} from "./pages/albums/albums.component";
import {RegisterComponent} from "./pages/register/register.component";
import {LoginComponent} from "./pages/login/login.component";
import {TracksComponent} from "./pages/tracks/tracks.component";
import {TrackHistoryComponent} from "./pages/track-history/track-history.component";

const routes: Routes = [
  {path: '', component: ArtistsComponent},
  {path: 'albums/:id', component: AlbumsComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'tracks/:id', component: TracksComponent},
  {path: 'track_history', component: TrackHistoryComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
