import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Track} from "../../models/track.model";
import {AppState} from "../../store/types";
import {Store} from "@ngrx/store";
import {fetchTracksByAlbumRequest} from "../../store/tracks.actions";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.sass']
})
export class TracksComponent implements OnInit {
  idAlbum: Observable<string>;
  tracks: Observable<Track[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.idAlbum = store.select(state => state.tracks.tracksByAlbumId);
    this.tracks = store.select(state => state.tracks.tracks);
    this.loading = store.select(state => state.tracks.fetchLoading);
    this.error = store.select(state => state.tracks.fetchError);
  }

  ngOnInit(): void {
    const idAlbum = this.route.snapshot.params['id'];
    this.store.dispatch(fetchTracksByAlbumRequest({idAlbum}));
  }

  addToTrackHistory(idTrack: string) {
  }
}
