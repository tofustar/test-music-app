import {TracksState} from "./types";
import {createReducer, on} from "@ngrx/store";
import {
  fetchTracksByAlbumFailure,
  fetchTracksByAlbumRequest, fetchTracksByAlbumSuccess,
  fetchTracksFailure,
  fetchTracksRequest,
  fetchTracksSuccess
} from "./tracks.actions";

const initialState: TracksState = {
  tracks: [],
  tracksByAlbumId: '',
  fetchLoading: false,
  fetchError: null,
};

export const tracksReducer = createReducer(
  initialState,
  on(fetchTracksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTracksSuccess, (state, {tracks}) => ({...state, fetchLoading: false, tracks})),
  on(fetchTracksFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchTracksByAlbumRequest, (state, {idAlbum}) => ({...state, fetchLoading: true, tracksByAlbumId: idAlbum})),
  on(fetchTracksByAlbumSuccess, (state, {tracks}) => ({...state, fetchLoading: false, tracks})),
  on(fetchTracksByAlbumFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),
)