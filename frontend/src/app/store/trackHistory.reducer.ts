import {TrackHistoryState} from "./types";
import {createReducer, on} from "@ngrx/store";
import {addToTrackHFailure, addToTrackHRequest, addToTrackHSuccess} from "./trackHistory.actions";

const initialState: TrackHistoryState = {
  trackHistories: [],
  idTrack: '',
  createLoading: false,
  createError: null,
};

export const trackHistoriesReducer = createReducer(
  initialState,
  on(addToTrackHRequest, (state, {idTrack}) => ({...state, createLoading: true, idTrack})),
  on(addToTrackHSuccess, (state) => ({...state, createLoading: false})),
  on(addToTrackHFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),
)