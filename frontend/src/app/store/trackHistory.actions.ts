import {createAction, props} from "@ngrx/store";

export const addToTrackHRequest = createAction('[Track History] Request', props<{idTrack: string}>());
export const addToTrackHSuccess = createAction('[Track History] Success');
export const addToTrackHFailure = createAction('[Track History] Failure', props<{error:string}>());