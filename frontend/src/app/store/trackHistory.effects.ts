import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {TrackHistoryService} from "../services/trackHistory.service";
import {addToTrackHFailure, addToTrackHRequest, addToTrackHSuccess} from "./trackHistory.actions";
import {catchError, map, mergeMap, NEVER, of, withLatestFrom} from "rxjs";
import {Store} from "@ngrx/store";

@Injectable()

export class TrackHistoryEffects {
  constructor(
    private actions: Actions,
    private trackHistoryService: TrackHistoryService,
    private store: Store,
  ) {
  }

  addToTrackHistory = createEffect(() => this.actions.pipe(
      ofType(addToTrackHRequest),
      withLatestFrom(this.store.select(state =>  state.users.user)),
      mergeMap(([{idTrack}, user]) => {
        if (user) {
          return this.trackHistoryService.addToTrackHistory(user.token, idTrack).pipe(
            map(() => addToTrackHSuccess),
            catchError(() => of(addToTrackHFailure({error: 'Something went wrong'})))
          );
        }
        return NEVER;
      })
    )
  )
}