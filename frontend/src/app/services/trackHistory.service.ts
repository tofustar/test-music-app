import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment as env} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class TrackHistoryService {
  constructor(private http: HttpClient) {}

  addToTrackHistory(token: string, idTrack: string) {
    return this.http.post(env.apiUrl + '/track_history', idTrack, {
      headers: new HttpHeaders({'Authorization': token})
    });
  }
}