import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Album, ApiAlbumData} from "../models/album.model";
import {environment as env} from "../../environments/environment";
import {map} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class AlbumsService {

  constructor(private http: HttpClient) {}

  getAlbums() {
    return this.http.get<ApiAlbumData[]>(env.apiUrl + '/albums').pipe(
      map(response => {
        return response.map(albumData => {
          return new Album(
            albumData._id,
            albumData.name,
            albumData.artist,
            albumData.year,
            albumData.image,
          );
        });
      })
    )
  }

  getAlbumsByArtist(artistId: string) {
    return this.http.get<ApiAlbumData[]>(env.apiUrl + '/albums?artist=' + artistId);
  }
}