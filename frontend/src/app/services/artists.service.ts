import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ApiArtistData, Artist} from "../models/artist.model";
import {environment as env} from "../../environments/environment";
import {map} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class ArtistsService {

  constructor(private http: HttpClient) {}

  getArtists() {
    return this.http.get<ApiArtistData[]>(env.apiUrl + '/artists').pipe(
      map(response => {
        return response.map(artistData => {
          return new Artist(
            artistData._id,
            artistData.name,
            artistData.image,
            artistData.info,
          );
        });
      })
    );
  }
}