import {Injectable} from "@angular/core";
import {LoginUserData, RegisterUserData, User} from "../models/user.model";
import {environment as env} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})

export class UsersService {
  constructor(private http: HttpClient) {}

  registerUser(userData: RegisterUserData) {
    return this.http.post<User>(env.apiUrl + '/users', userData);
  }

  login(userData: LoginUserData) {
    return this.http.post<User>(env.apiUrl + '/users/sessions', userData);
  }

  logout(token: string) {
    return this.http.delete(env.apiUrl + '/users/sessions', {
      headers: new HttpHeaders({'Authorization': token})
    });


  }
}